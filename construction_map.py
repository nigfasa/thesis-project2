from filtering import save_trayectories, clean_data
import os
import psycopg2
import sys
import pandas as pd
import pandas.io.sql as sqlio
from shutil import copyfile, move
import csv
import time


class ConstructionMap():
    
    def __init__(self, test="", steps=60, size=2000, max_average=3, rdp_value=1, esp=100):
        self.dir_name = ""
        self.test = test
        self.steps = steps
        self.size = size
        self.max_average = max_average
        self.rdp_value = rdp_value
        self.esp = esp
        # you need to change the name experiment3 and the database information
        self.credentials = {
            "user": 'nigfasa',
            "password": '',
            "host": "127.0.0.1",
            "port": "5432",
            "database": "maps"}

    def connect_database(self):
        # INPUT:
        # size = amount of data to get from database (default 2000)
        # if size = None or False or "", takes all the data
        #
        # INPUT TESTING:
        # test = path of the csv

        # Important: dir_name is not  directory name but the names of the directories that are going to be created
        print(self.test)
        if self.test:
            dat = pd.read_csv((str(self.test)))
            new_data = clean_data(
                dat, steps=self.steps, max_average=self.max_average, rdp_value=self.rdp_value)
            self.dir_name = str(id(new_data))
            save_trayectories(new_data, dir_name=self.dir_name, steps=self.steps)

        else:
            
            sql = "select * from experiment3;"
            if self.size:
                sql = "select * from experiment3 limit "+str(self.size)+";"

            try:
                connection = psycopg2.connect(**self.credentials)

                cursor = connection.cursor()
                # Print PostgreSQL Connection properties
                print(connection.get_dsn_parameters(), "\n")
                # Print PostgreSQL version
                cursor.execute("SELECT version();")
                record = cursor.fetchone()
                print("You are connected to - ", record, "\n")
                dat = sqlio.read_sql_query(sql, connection)
                new_data = clean_data(
                    dat, steps=self.steps, max_average=self.max_average, rdp_value=self.rdp_value)
                self.dir_name = str(id(new_data))
                save_trayectories(new_data, dir_name=self.dir_name, steps=self.steps)

            except (Exception, psycopg2.Error) as error:
                print("Error while connecting to PostgreSQL", error)

            finally:
                # closing database connection.
                if(connection):
                    cursor.close()
                    connection.close()
                    print("PostgreSQL connection is closed")


    def ahmed_algorithm(self):
        dir_name = self.dir_name
        #print(dir_name)
        input_path = "input_"+str(dir_name)
        output_path = "output_"+str(dir_name)+"/"

        if not(os.path.isdir('./Ahmed/'+input_path+'/')):
            os.mkdir('Ahmed/'+input_path)
        if not(os.path.isdir('./Ahmed/'+output_path)):
            os.mkdir('Ahmed/'+output_path[:-1])

        copyfile(os.getcwd()+'/trajectory_'+str(dir_name)+'/' +
                 "saved_trajectories.csv", "./Ahmed/"+input_path+"/init_result.csv")
        
        # creation of the script, be aware, for some reason if the file doesn't exist
        # before running this it will throw a permission error
        # this doesn't allow to dynamically create them (or at least i wasn't able to do so)
        path = 'Ahmed/12.sh'

        with open(path, 'w+') as f:
            basis = [
                '#!/usr/bin/env bash',
                '\n',
                'echo 12321312'
                '\n',
                '#To Compile',
                '\n',
                'cd Ahmed',
                '\n',
                'javac -d bin  src/mapconstruction2/*.java'
                '\n',
                'INPUT_PATH='+'"'+input_path+'"',
                '\n',
                'OUTPUT_PATH='+'"'+output_path+'"',
                '\n',
                'EPS='+str(self.esp),
                '\n',
                'HAS_ALTITUDE=false',
                '\n',
                'ALT_EPS=1.0',
                '\n',
                'java -cp bin/ mapconstruction2.MapConstruction $INPUT_PATH $OUTPUT_PATH $EPS $HAS_ALTITUDE $ALT_EPS'
                '\n',
            ]
            f.writelines(basis)
        time.sleep(3)
        stream = os.popen('chmod +rx '+path)
        stream = os.popen('./'+path).read()
        print(stream)
        time.sleep(5)
        move("Ahmed/"+input_path, 'trajectory_'+str(dir_name))
        move("Ahmed/"+output_path[:-1], 'trajectory_'+str(dir_name))
        

        with open("./trajectory_"+str(dir_name)+"/"+output_path+"edges.txt") as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',')
            csv1 = list(csv_reader)
            with open("./trajectory_"+str(dir_name)+"/"+output_path+"vertices.txt") as csv_file2:
                csv_reader2 = csv.reader(csv_file2, delimiter=',')
                csv2 = list(csv_reader2)

                dictt = {}
                for i in csv2:
                    dictt[i[0]] = [i[1], i[2]]
                for i in csv1:
                    i[1] = dictt[i[1]]
                    i[2] = dictt[i[2]]
                #print(csv1)

                with open("./trajectory_"+str(dir_name)+"/"+output_path+"final.txt", mode='w') as csv_file3:
                    csv_writer = csv.writer(
                        csv_file3, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
                    for i in csv1:
                        csv_writer.writerow(
                            [i[0], i[1][0], i[1][1], i[2][0], i[2][1]])


#contruction_map = ConstructionMap(test='test_base200k.csv')
contruction_map = ConstructionMap(test=False)
contruction_map.connect_database()
contruction_map.ahmed_algorithm()
