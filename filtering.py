# -*- coding: utf-8 -*-
import math
import psycopg2
import copy
import pandas as pd
import sys
import pandas.io.sql as sqlio
import numpy as np
from os import listdir, path, mkdir, getcwd
import time
from scipy import stats


# RDP algorithm as long as the rdp package is not iterative.
# See https://github.com/fhirschmann/rdp/issues/5
def _DouglasPeucker(points, startIndex, lastIndex, epsilon):
    stk = []
    stk.append([startIndex, lastIndex])
    globalStartIndex = startIndex
    bitArray = np.ones(lastIndex-startIndex+1, dtype=bool)

    while len(stk) > 0:
        startIndex = stk[-1][0]
        lastIndex = stk[-1][1]
        stk.pop()

        dmax = 0.
        index = startIndex

        for i in range(index+1, lastIndex):
            if bitArray[i - globalStartIndex]:
                d = PointLineDistance(points[i], points[startIndex], points[lastIndex])
                if d > dmax:
                    index = i
                    dmax = d
        if dmax > epsilon:
            stk.append([startIndex, index])
            stk.append([index, lastIndex])
        else:
            for i in range(startIndex + 1, lastIndex):
                bitArray[i - globalStartIndex] = False
    return bitArray


def rdp(points, epsilon):
    """
    Ramer-Douglas-Peucker algorithm
    """
    bitArray = _DouglasPeucker(points, 0, len(points)-1, epsilon)
    resList = []
    for i in range(len(points)):
        if bitArray[i]:
            resList.append(points[i])
    return np.array(resList)


def rdp_mod(points, timestamps, epsilon):
    """
    Ramer-Douglas-Peucker algorithm, this accept a third value which is timestamp
    """
    bitArray = _DouglasPeucker(points, 0, len(points)-1, epsilon)
    resList =  []
    for i in range(len(points)):
        if bitArray[i]:
            resList.append([points[i][0], points[i][1], timestamps[i][0]])
    return np.array(resList)


def PointLineDistance(point, start, end):
    if np.all(np.equal(start, end)) :
        return np.linalg.norm(point, start)
    n = abs((end[0] - start[0]) * (start[1] - point[1]) - (start[0] - point[0]) * (end[1] - start[1]))
    d = math.sqrt((end[0] - start[0]) * (end[0] - start[0]) + (end[1] - start[1]) * (end[1] - start[1]))
    return n/d


def haversine(coord1, coord2):
    """
    Haversine distance in meters for two (lat, lon) coordinates
    """
    lat1, lon1 = coord1
    lat2, lon2 = coord2
    radius = 6371000 # mean earth radius in meters (GRS 80-Ellipsoid)
    dlat = math.radians(lat2-lat1)
    dlon = math.radians(lon2-lon1)
    a = math.sin(dlat/2) * math.sin(dlat/2) + math.cos(math.radians(lat1)) \
        * math.cos(math.radians(lat2)) * math.sin(dlon/2) * math.sin(dlon/2)
    c = 2 * math.atan2(math.sqrt(a), math.sqrt(1-a))
    d = radius * c
    return d

# End of RDP algorithm 






start = time.time()

def clean_data(dt, steps=60, max_average=3, rdp_value=1):
    '''
    Description: Here it data is filtered and The RDP algorithm is used. 

    Input: data from the database.
    Output: Filtered data.

    Parameters: 
    - RDP parameter.ß
    - Error group parameter: In order to clean the data from higly erratic error this was created.

    Testing: Offers the testing option. It is advised to use it to test the parameters. 
    '''


    # filter the data
    keep_columns = ['latitude', 'longitude', 'ts', 'time_zone', 'device_id']
    new_f = dt[keep_columns]


    # group them into unique trajectories
    f_grouped = dt.groupby(['device_id', 'time_zone'])
    keys = list(f_grouped.groups.keys())

    # create extra columns with the difference in distance and time between a point
    # and its sucessor.
    frames2 = []
    douglas_time = 0
    all_values=[]
    all_count=[]
    for i in keys:
        all_distances = []
        all_times = []
        all_distances.append(0)
        all_times.append(0)
        k_0 =  f_grouped.get_group(i)[['latitude', 'longitude', 'ts']]
        k_1 = list(zip(list(k_0['latitude']), list(k_0['longitude'])))
        k_t = list(k_0['ts'])
        for j,k in zip(k_1, k_1[1:]):
            distance_m = np.sqrt((j[0]-k[0])**2 + ((j[1]-k[1])**2))
            all_distances.append(distance_m)
        for j,k in zip(k_t, k_t[1:]):
            time_diff = abs(int(k)-int(j))
            all_times.append(time_diff)
        a = f_grouped.get_group(i).apply(lambda x: x)
        a.insert(2, "dif_ts", all_times, True)
        a.insert(2, "dif_dis", all_distances, True)
        
        # trajectories that are too long are splited into smaller ones:
        # In order to avoid cicles, and 
        # if a whole trajectory as too much error to erase the minimun amount of data. 
        c = copy.copy(a)
        # IMPORTANT: this value can be changed to improve results. 
        steps = steps
        divisions = [c[x:x+steps] for x in range(0, len(c), steps)]


        for a in divisions:
            # knowing that a person walks normally at 1m/s
            # if the average of a set is bigger than 3 the set has too many errors to be a good sample

            # IMPORTANT: this value can be changed to improve results. 
            max_average = max_average

            ave = a['dif_dis'].mean(skipna=True)
            if ave > max_average:
                continue
            all_values.append(ave)
            coun = a['dif_dis'].count()
            if coun < 20:
                continue
            all_count.append(coun)


            # This erases points that go too fast and too slow. This 
            a = a[a['dif_ts']<1500]
            a = a[a['dif_ts']>100]
            a = a[a['dif_dis']<1.4]
            a = a[a['dif_dis']>0.1]
            k_0 = a[['longitude', 'latitude']].values
            k_t = a[['ts']].values



            # The RDP Algorithm is used here
            # IMORTANT: this value can be changed. 
            rdp_value = rdp_value

            start_douglas = time.time()
            new_f = rdp_mod(k_0, k_t, rdp_value)
            end_douglas = time.time()
            douglas_difference =  end_douglas-start_douglas
            douglas_time += douglas_difference
            

            new_f = new_f.tolist()
            lon = []
            lat = []
            tps = []
            for m in new_f:
                lon.append(m[0])
                lat.append(m[1])
                tps.append(m[2])
            b = pd.DataFrame({'longitude': lon, 'latitude': lat, 'ts': tps})

            frames2.append(b)

    
    '''
    Note:
    # the folling code helps to see an stadistical analysis of the data

    print('start size:')
    print(len(dt))
    print(dt.describe())

    a_v = pd.DataFrame(all_values)
    print(a_v.describe())
    a_v = pd.DataFrame(all_count)
    print(a_v.describe())
    print(type(frames2))
    print(frames2)
    print('Douglas total time: {}'.format(str(douglas_time)))
    '''

    return(frames2)


'''
Test time:
end = time.time()
print(end - start)
'''

def save_trayectories(list_dt, dir_name= "", steps=60):
    #start = time.time()
    
    mkdir(getcwd()+'/trajectory_'+str(dir_name))
    key = 1
    frames = []
    for i in list_dt:
        steps = int(steps)
        group = i
        chunks = [group[x:x+steps] for x in range(0, len(group), steps)]
        for j in chunks:
            key_index  =  [key]*len(j)
            j.insert(0, "key_index", key_index, True)
            key+=1
            frames.append(j)
    result = pd.concat(frames)    
    path_going = getcwd()+'/trajectory_'+str(dir_name)+'/'+str('saved_trajectories')+'.csv'
    result[['key_index','latitude', 'longitude', 'ts']].to_csv(path_going, header=None, index=None, sep=',', mode='a') 
    #print('Size of the final file: ' + str(len(result)))
    #end = time.time()
    #print("cortar en pedazos: "+ str(end-start))