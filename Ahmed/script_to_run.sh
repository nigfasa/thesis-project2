#!/usr/bin/env bash
#To Compile:
#CODE_PATH="/Users/nigfasa/IdeaProjects/mapconstruction/algorithms/Ahmed" #path to the MapConstruction folder.
cd "Ahmed"
echo 123456123
echo $CODE_PATH
echo $PWD
javac -d bin  src/mapconstruction2/*.java
#To Run:
#INPUT_PATH="inD100s60s400kc60"
#OUTPUT_PATH="outD100s60s400kc60e800/" #path to the folder where output will be written
INPUT_PATH="I321"
OUTPUT_PATH="O231/"
mkdir $INPUT_PATH
mkdir $OUTPUT_PATH
EPS=100 #epsilon in meters
HAS_ALTITUDE=false #if input file has altitude information
ALT_EPS=1.0 #minimum altitude difference in meters between two streets
echo $PWD
java -cp bin/ mapconstruction2.MapConstruction $INPUT_PATH $OUTPUT_PATH $EPS $HAS_ALTITUDE $ALT_EPS



