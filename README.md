# Main project
The entire project is divided on two main parts: 

 1. **Ahmed algorithm (modification):** Based on the work of Ahmed and Wenk. It has improved performance to the original regarding data management.
 2. **Filtering:** Here it is described the necessary corrections that the algorithm needs for working correctly.

> It is important to mention that both of them work independenly from each other. At the start of this document an explanation of both of them is offered. 

**Note:** On neither of those document it is described how to implement the database. Nevertheless it is described the necessary ESPG projections for it to work here:
```
insert into experiment2(device_id, time_zone, longitude, latitude, geom, ts) 
 select device_id, time_zone, ST_X(st_transform), ST_Y(st_transform), st_transform, ts from
(SELECT ST_Transform(ST_GeomFromText('POINT('||inn.longitude||' '||inn.latitude||')',4326),3035), inn.ts, inn.device_id, inn.time_zone
from (Select longitude, latitude, ts, device_id, time_zone from base_map2) as inn) as inn2;
```

This will change the data to one that works.

# Filtering

It is highly advised to read filtering.py. The varible explanations are there.

The most important functions to take in account are:

- **clean_data:** It filtears and cleans the data, this is the most important file of the filtering phase.
- **_DouglasPeucker:** Here is the implementation of the  RDP algorithm.
- **save_trayectories:** It saves the document into a csv.

# Ahmed Algorithm 

Can be found in the Ahmed directory and used running a script. The file *script_to_run.sh* is an example of that. 

**Input file format:**  csv file like "trajectory_id x y timestamp". Note: The longer the average trajectory the fastest and better final algorithm, but only it doesn't have too much highly erratic errors. In some implementations 2 point trajectories yielded good results in exchange of time. 

**Output file format:**  
vertex file: "vertexid x y z"
edge file: "edgeid vertexid1 vertexid2"
final file: graph of vertex connected with each other.

# How to run the program? 

The program is fairly easy to use and versatile. You need to go to the file *construction_map.py*, which has a class named *ConstructionMap*. The idea is that each instances of that class could be parallelized. Please read the files for further explanation of its variables.

The whole process is summarized here.
![](diagram.jpg)

Once an instance is created it follows this workflow:

 1. **Database or TestFile:** The program connects to the database of selects a test_file. Once that is done, the following part starts. 
 2. **Filtering:** The necesary data modifications are done plus the RDP.
 3. **Script creating and Ahmed Algorithm:** A script is created with the necessary specifications for Ahmed's algorithm and the data is saved as described on the picture. 

**Note:** there are several problems with the permision of the use and creation of the *sh* file. Please be aware of how delicate it is.

**Note2:** the file *save_size_test.py* is useful to create testing files. 



